// import './plugins/axios'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '../src/css/styles.css'


import '../src/tailwind.css'
    
createApp(App).use(store).use(router).mount('#app')