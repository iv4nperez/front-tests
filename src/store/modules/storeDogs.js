import { HttpRequest } from '../../scripts/HTTPRequest'
import { toLiked, getListDogs, saveHistoryLocalStorage,getListHistory } from '../../scripts/tools'

export default {
    namespaced:true,
    state:{
        resultSearch: [],
        resultPaginado:[],
        img:'https://i.stack.imgur.com/y9DpT.jpg',
        loading: {
            value: false,
            text: ''
        },
        listLike: [],
        history:[ ]
    },
    mutations:{
        SET_SEARCH_DOGS(state, payload){
            state.resultSearch = payload;
        },
        SET_LOADING(state, payload){
            state.loading = payload;
        },
        SET_LIKE_DOGS(state, payload ){
            const result = toLiked(payload);
            state.listLike = result;
        },
        GET_LIST_DOGS_LIKE(state){
            state.listLike = getListDogs();
        },
        SET_HISTORY_DOGS(state, payload){
            const result = saveHistoryLocalStorage( payload );
            state.history = result;
        },
        GET_HISTORY_DOGS(state){
            state.history = getListHistory();
        },
    },
    actions:{
        //se realiza la busqueda del perrito
        async getSearchDogs({ commit, state }, payload ){
            commit('SET_LOADING', {
                value: true,
                text: 'Buscando a tu perrito favorito ...'
            });

            let listDogs = [];
            const theDog = await HttpRequest(`/v1/breeds/search?q=${ payload }`);
            
            // se evalua que traiga datos
            if( theDog.length > 0 ){
                for (const item of theDog) {
                    //se verifica que exista reference_image_id para hacer optra peticion que nos adjunta la imagen del perrito
                    if( item?.reference_image_id ){

                        const { reference_image_id } = item;
                        const { url } = await HttpRequest(`/v1/images/${ reference_image_id }`);
                        item.img = url;
                    }else{
                        item.img = state.img;
                    }

                    listDogs = [...listDogs, ...[item] ];
                } 

            }

            commit('SET_SEARCH_DOGS', listDogs );     
            
            commit('SET_LOADING', {
                value: false,
                text: ''
            });
        },
    }
}