import { createStore } from 'vuex'
import storeDogs from "../store/modules/storeDogs.js";

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    storeDogs
  }
})
