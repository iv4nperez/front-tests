
// toLiked esta funcion agrega un id a la liat de perrito
export const toLiked = ( id ) => {
  
    let otherArr = [];
    const array = JSON.parse(localStorage.getItem('toLikeDogs'));

    if(array){

        if(array.includes(id)){
            let i = array.indexOf( id );
            array.splice( i, 1 );
            localStorage.setItem('toLikeDogs',JSON.stringify(array))
            
            return array;
        }

        otherArr = [ ...array, id];
    }else{
        otherArr.push(id)
    }
    localStorage.setItem('toLikeDogs',JSON.stringify(otherArr))

    return otherArr;
} 
//getListDogs solo obtene la lista de perrtito
export const getListDogs = () => {

    const array = JSON.parse(localStorage.getItem('toLikeDogs'));
    if ( array ) {
        return array;
    }

    return [];
}
//getListHistory obtene la lista del historial del localstorage
export const getListHistory = () => {

    const array = JSON.parse(localStorage.getItem('historyTheDogs'));
    if ( array ) {
        return array;
    }
    return [];
}
//saveHistoryLocalStorage guarda un regisatro en el historial del local storage
export const saveHistoryLocalStorage = ( search = '' ) => {

    let otherArr = [];
    const array = JSON.parse(localStorage.getItem('historyTheDogs'));

    if(array){

        let item = {
            search: search,
            hour: new Date()
        };

        otherArr = [ ...array, item ];
    }else{
        otherArr.push({
            search: search,
            hour: new Date()
        })
    }
    localStorage.setItem('historyTheDogs',JSON.stringify(otherArr))

    return otherArr;

}
//formatDate formatea la fecha al formato necesario
export const formatDate = ( date = new Date(), format = 'YYYY.MM.DD HH:mm' ) => {

    return  moment( date ).locale('es').format( format );
 
}