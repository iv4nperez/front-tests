const BASE_URL = 'https://api.thedogapi.com';
const apiKey = '7bd6b415-14db-4665-a8f4-cb619a953260';


//HttpRequest realiza las peticiones http, se hace de esta manera para centralizar errores o algo mas.
export const HttpRequest = async ( url,params = {}, method = 'GET' ) =>  {

    // `/v1/breeds/search?q=${ this.model }`
    const theDog = await fetch( BASE_URL + url, {
        
        method: method ,
        headers:{
          'X-Api-Key': apiKey,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        params: params
    }).then( r => r.json());

    return theDog;

}