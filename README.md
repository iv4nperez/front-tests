Documentación de **Search the dogs**
En esta documentación encontraras todo lo necesario para poder correr el proyecto.
1. abrir el proyecto.
2. reinstalar **npm** con **npm install**.

Al reinstalar los paquetes de **npm** vamos a poder correr el proyecto con el siguiente comando: **npm run serve** 
En este pequeño proyecto se usa:

 - Vue 3
 - Vuex
 - Vue-Router
 
 Ahora al levantar el proyecto nos redireccionara a **Home** o **/**.
 En esta aplicación podremos realizar la búsqueda de raza de perritos la cuales vamos a poder ver en un **cardComponent** y al realizar búsquedas se registrara un historial y podremos guardar nuestros perros favoritos con un botón con un **icono de corazón** el cual se quedara activado o en color rojo para poder distinguir que perrito nos ha gustado

**DOCUMENTACION**
En este proyecto tenemos una sección de documentación para los componentes creados, puede ser que algunos estilos o clases de css no estén bien asignas ya que es mi primer contacto con **tailwind css** de lleno.
ruta: **http://localhost:8080/documentacion**
en caso de que uses otro localhost solo deberás agregar el nuevo localhost a esta ruta para poder ser redirigido.
**KEYS**
Las keys que se usan para consultar las APIS se encuentran **src > scripts > HTTPRequest.js** encontraras 2 constantes los cuales guardan las **keys** y la base url de la **API**

**TEST**
SI quieres correr las pruebas es necesario posicionarte en la carpeta del proyecto y ejecutar el siguiente comando: **npm run test:unit**

**COMENTARIOS EXTRAS**
En la parte de **src** > **script** > **tools.js** tendrás disponible el archivo **tools.js** en el cual van **funciones reutilizables**.
Cabe destacar que los componentes son una beta el cual puede ser que haya mas funcionalidades que se les puede agregar.