import { shallowMount } from '@vue/test-utils'
import WTextField from '@/components/WTextField'


describe('WTextField Component',() => {

    // let wrapper;
    // beforeEach(() => {
    //     wrapper = shallowMount( WLoading );
    // });

    test('debe de recibir correctamente las props', () => {
        
        let myProps = {
            value:'texto',
            colorOutline:'red',
            intensity:'800',
            label:'label',
            showIcon:true,
            message:'Test'
        }

        const wrapper = shallowMount( WTextField,{
            props:{
                value: myProps.value,
                colorOutline:myProps.colorOutline,
                intensity:myProps.intensity,
                label:myProps.label,
                showIcon:myProps.showIcon,
                message:myProps.message
            }   
        });


        const propsCard = wrapper.props();

        expect( myProps ).toEqual( propsCard );

    })

})