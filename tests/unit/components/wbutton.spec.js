import { shallowMount } from '@vue/test-utils'
import WButton from '@/components/WButton'

describe('WButton Component',() => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount( <WButton>texto</WButton> )
    });


    // test('debe de hacer match con el snapshot', () => {
        
    //     const wrapper = shallowMount( <WButton></WButton> );

    //     expect( wrapper.html() ).toMatchSnapshot();
        
    // });
    
    test('debe de mostrar correctemente el texto del span', () => {

       
        const valueSpan = wrapper.find('[data-test-id="span-text"]').text()
        expect( valueSpan ).toBe('texto');
    });

    test('colorborde debe de ser un string', () => {
        
        const { colorBorder } = wrapper.props();

        expect( typeof colorBorder ).toBe( 'string' );
    })

    test('prop colorborder debe de ser gray-3oo por default', () => {
        
        const { colorBorder } = wrapper.props();
        expect( colorBorder ).toBe( 'gray-300' );

    })
    
    test('verificar que reciba la prop correctamente', async  () => {

        const color = 'red-300'
        const wrapper = shallowMount( <WButton>texto</WButton>, {
            props:{
                colorBorder:color
            },
        });
        
        const { colorBorder } = wrapper.props();
        expect( colorBorder ).toBe( color );

    });  

});
