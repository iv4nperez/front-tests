import { shallowMount } from '@vue/test-utils'
import WLoading from '@/components/WLoading'


describe('WLoading Component',() => {

    // let wrapper;
    // beforeEach(() => {
    //     wrapper = shallowMount( WLoading );
    // });

    test('debe de recibir correctamente las props', () => {
        
        let myProps = {
            loading:true,
            textLoading:'Cargando ...',
        }

        const wrapper = shallowMount( WLoading,{
            props:{
                loading: myProps.loading,
                textLoading: myProps.textLoading
            }   
        });


        const propsLoading = wrapper.props();

        expect( myProps ).toEqual( propsLoading );

    })

})