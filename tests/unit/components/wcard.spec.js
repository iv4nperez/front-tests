import { shallowMount } from '@vue/test-utils'
import WCard from '@/components/WCard'


describe('WCard Component',() => {

    // let wrapper;
    // beforeEach(() => {
    //     wrapper = shallowMount( WCard );
    // });

    test('debe de recibir correctamente las props', () => {
        
        let myProps = {
            title:'texto',
            img:'http://t1.gstatic.com/licensed-image?q=tbn:ANd9GcQOJuMI5YQam1C9mIbkHHg1T2KxGJAUNAq8FPLvtBlnKuFIeeSCkv0jcF6pHPDzZ4PuKDq0tDgb5-r8K9Nl1e0',
            clickButton: ()=> {},
            showBottonAction: true,
            activeBottonColor: true
        }

        const wrapper = shallowMount( WCard,{
            props:{
                title: myProps.title,
                img: myProps.img,
                clickButton: myProps.clickButton,
                showBottonAction : myProps.showBottonAction,
                activeBottonColor: myProps.activeBottonColor
            }
        });


        const propsCard = wrapper.props();

        expect( myProps ).toEqual( propsCard );

    })

    test('debe de mostar correctamente el titulo del card', () => {
        
        let myProps = {
            title:'texto',
            img:'http://t1.gstatic.com/licensed-image?q=tbn:ANd9GcQOJuMI5YQam1C9mIbkHHg1T2KxGJAUNAq8FPLvtBlnKuFIeeSCkv0jcF6pHPDzZ4PuKDq0tDgb5-r8K9Nl1e0',
        }

        const wrapper = shallowMount( WCard,{
            props:{
                title: myProps.title,
                img: myProps.img,
            }
        });

        const title = wrapper.find('[data-test-id="title-card"]').text()
        expect( title ).toBe( myProps.title )

    })
    
    



})